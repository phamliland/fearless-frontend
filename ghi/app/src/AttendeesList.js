
function AttendeesList(props) {
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th className="d-flex flex-row-reverse justify-content-center align-items-center">Conference</th>
            </tr>
            </thead>
            <tbody>
                {props.attendees.map(attendee => {
                    return (
                        <tr key={attendee.href}>
                            <td>{ attendee.name }</td>
                            <td className="d-flex flex-row-reverse justify-content-center align-items-center">{ attendee.conference }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default AttendeesList;
